/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by qasim on 7/25/16.
	 */
	angular
	    .module("angular-app",[])
	    .controller("LoginController",["$scope",__webpack_require__(1)])
	    .directive("nplogin",[__webpack_require__(2)]);


/***/ },
/* 1 */
/***/ function(module, exports) {

	/**
	 * Created by qasim on 7/25/16.
	 */
	module.exports=function ($scope) {
	    $scope.name="Hello World !";
	}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Created by qasim on 7/25/16.
	 */
	module.exports=function () {
	    return{
	        restrict:"E",
	        replace:true,
	        template:__webpack_require__(3),
	        link:function () {

	        }
	    };
	}


/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = "<div>\n    <form name=\"login-form\">\n        <label>Username *</label>\n        <input name=\"username\" id=\"username\"/>\n        <label>Password *</label>\n        <input name=\"password\" id=\"password\"/>\n    </form>\n</div>\n\n\n\n";

/***/ }
/******/ ]);