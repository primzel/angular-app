/**
 * Created by qasim on 7/25/16.
 */
angular
    .module("angular-app",[])
    .controller("LoginController",["$scope",require('./controllers/LoginController')])
    .directive("nplogin",[require("./directives/LoginDirective")]);
