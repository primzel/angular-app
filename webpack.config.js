/**
 * Created by qasim on 7/25/16.
 */
module.exports={
    entry:"./src/app.js",
    output:{
        path:"./public",
        filename:"app.bundle.js"
    },
    module:{
        loaders:[
            {
                test:/\.html$/,
                loader:"html"
            }
        ]
    }
}
